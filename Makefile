CC = gcc
OBJS = wpa_actiond.o
LINKOBJS = wpa_ctrl/wpa_ctrl.o wpa_ctrl/os_unix.o $(OBJS)
TARGET = wpa_actiond
LIBS =

all: $(TARGET)

wpactrl:
	make -C wpa_ctrl

$(TARGET): wpactrl $(OBJS)
	$(CC) $(LINKOBJS) $(CFLAGS) $(LDFLAGS) $(LIBS) -o $(TARGET)

clean:
	make -C wpa_ctrl clean
	rm -f $(OBJS) $(TARGET)

%.o: %.c
	$(CC) -c $(CFLAGS) $< -o $@
